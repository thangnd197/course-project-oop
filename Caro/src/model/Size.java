package model;

public class Size {
	public static final int CHESS_WIDTH = 20;
	public static final int CHESS_HEIGHT = 20;
	
	public static final int TILE_WIDTH = 30;
	public static final int TILE_HEIGHT = 30;
}
