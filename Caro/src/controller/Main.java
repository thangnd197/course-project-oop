package controller;

import java.awt.EventQueue;

import model.Size;
import view.ViewBoard;

public class Main {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {			public void run() {
				try {
					ViewBoard frame = new ViewBoard(Size.CHESS_WIDTH, Size.CHESS_HEIGHT, Size.TILE_WIDTH, Size.TILE_HEIGHT);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
