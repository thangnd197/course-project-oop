package com.chess.engine.pieces;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.chess.engine.Alliance;
import com.chess.engine.board.Board;
import com.chess.engine.board.BoardUtils;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import com.google.common.collect.ImmutableList;

public class Knight extends Piece{
	private final static int[] CANDIDATE_MOVE_COORDINATES = {-17, -15, -10, -6, 6, 10, 15, 17};
	Knight(int piecePosition, Alliance pieceAlliance) {
		super(piecePosition, pieceAlliance);
	}
	
	@Override
	public Collection<Move> calculateLegalMoves(final Board board) {
		
		final List<Move> legalMoves = new ArrayList<>();
		
		for(final int currentCandinateOffset: CANDIDATE_MOVE_COORDINATES) {
			final int candidateDestinationCoordinate = this.piecePosition + currentCandinateOffset;
			if(BoardUtils.isValidTileCoordinate(candidateDestinationCoordinate)) {
				if(isFirstColumnExclusion(this.piecePosition, currentCandinateOffset)||
						isSecondColumnExclustion(this.piecePosition, currentCandinateOffset)||
						isSeventhColumsExclusion(this.piecePosition, currentCandinateOffset)||
						isEighthColumsExclusion(this.piecePosition, currentCandinateOffset)) {
					continue;
				}
				final Tile candidateDestinationTile = board.getTile(candidateDestinationCoordinate);
				if(!candidateDestinationTile.isTileOccupied()) {
					legalMoves.add(new Move.MajorMove(board, this, candidateDestinationCoordinate));
				}else {
					final Piece pieceAtDestination = candidateDestinationTile.getPiece();
					final Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();
					if(this.pieceAlliance != pieceAlliance) {
						legalMoves.add(new Move.AttackMove(board, this, candidateDestinationCoordinate, pieceAtDestination));
					}
				}
			}
		}
		return ImmutableList.copyOf(legalMoves);
	}
	
	public static boolean isFirstColumnExclusion(final int currentPosition, final int candinateOffset) {
		return BoardUtils.FIRST_COLUMN[currentPosition] && ((candinateOffset == -17) ||
				(candinateOffset == -10) || (candinateOffset == 6) || (candinateOffset == 15));
	}
	
	public static boolean isSecondColumnExclustion(final int currentPosition, final int candidateOffset) {
		return BoardUtils.SECOND_COLUMN[currentPosition] && (candidateOffset == -10 || candidateOffset == 6);
	}

	public static boolean isSeventhColumsExclusion(final int currentPosition, final int candinateOffset) {
		return BoardUtils.SEVENTH_COLUMN[currentPosition] && (candinateOffset == -6 || candinateOffset == 10);
	}
	
	public static boolean isEighthColumsExclusion(final int currentPosition, final int candinateOffset) {
		return BoardUtils.EIGHT_COLUMN[currentPosition] && (candinateOffset == -15 || candinateOffset == -6 ||
				candinateOffset == 10 || candinateOffset == 17);
	}
}






















