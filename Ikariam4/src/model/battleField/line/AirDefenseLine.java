package model.battleField.line;

import model.army.Army;
import model.army.inforOfUnit;
import model.battleField.Cell;

public class AirDefenseLine extends Line {

  public AirDefenseLine(int theNumberOfCell, int sizeOfCell, int sizeOfArray, Army army) {
    super(theNumberOfCell, sizeOfCell, sizeOfArray, army);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void sort(Army army) {
    
    for (int i = 0; i < theNumberOfCell; i++) {
      
      if (army.getGyrocopters().size() 
          / (sizeOfCell / inforOfUnit.GYROCOPER[2]) > 0) {
        
        cells[i] = new Cell();
        
        for (int x = 0; x < sizeOfCell / inforOfUnit.GYROCOPER[2]; x++) {
          
          cells[i].getUnits().add(army.getGyrocopters().remove(0));
          
        }
        
      } else if (army.getGyrocopters().size()
          % (sizeOfCell / inforOfUnit.GYROCOPER[2]) > 0) {
        
        cells[i] = new Cell();
        
        for (int x = 0; x < army.getGyrocopters().size(); x++) {
          
          cells[i].getUnits().add(army.getGyrocopters().remove(0));
          
        }
      }
    }    
  }



}
