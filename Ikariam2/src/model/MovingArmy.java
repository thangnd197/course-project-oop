package model;

import model.unit.Archer;
import model.unit.BalloonBombardier;
import model.unit.Catapult;
import model.unit.Cook;
import model.unit.Doctor;
import model.unit.Gyrocopter;
import model.unit.Hoplite;
import model.unit.Marksman;
import model.unit.Mortar;
import model.unit.Ram;
import model.unit.Slinger;
import model.unit.Spartans;
import model.unit.Spearman;
import model.unit.SteamGiant;
import model.unit.Swordman;

public class MovingArmy extends Army{
	


	public MovingArmy(int amountArcher, int amountBalloonBombardier, int amountCatapult, int amountGyrocopter,
			int amountHoplite, int amountMarksman, int amountMortar, int amountRam, int amountSlinger,
			int amountSpartans, int amountSpearman, int amountSteamGiant, int amountSwordman, int amountCook,
			int amountDoctor, Archer archer, BalloonBombardier balloonBombardier, Catapult catapult,
			Gyrocopter gyrocopter, Hoplite hoplite, Marksman marksman, Mortar mortar, Ram ram, Slinger slinger,
			Spartans spartans, Spearman spearman, SteamGiant steamGiant, Swordman swordman, Cook cook, Doctor doctor) {
		super(amountArcher, amountBalloonBombardier, amountCatapult, amountGyrocopter, amountHoplite, amountMarksman,
				amountMortar, amountRam, amountSlinger, amountSpartans, amountSpearman, amountSteamGiant, amountSwordman,
				amountCook, amountDoctor, archer, balloonBombardier, catapult, gyrocopter, hoplite, marksman, mortar, ram,
				slinger, spartans, spearman, steamGiant, swordman, cook, doctor);
		// TODO Auto-generated constructor stub
	}

	public int calculateSpeed() {
		
		return 0;
	}
	
}
