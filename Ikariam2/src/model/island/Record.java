package model.island;

import java.util.ArrayList;
import java.util.List;

public class Record {
	private List<Integer> listIndexArcher = new ArrayList<Integer>();
	private List<Integer> listIndexBalloonBombardier = new ArrayList<Integer>();
	private List<Integer> listIndexCatapult = new ArrayList<Integer>();
	private List<Integer> listIndexGyrocopter = new ArrayList<Integer>();
	private List<Integer> listIndexHoplite = new ArrayList<Integer>();
	private List<Integer> listIndexMarksman = new ArrayList<Integer>();
	private List<Integer> listIndexMortar = new ArrayList<Integer>();
	private List<Integer> listIndexRam = new ArrayList<Integer>();
	private List<Integer> listIndexSlinger = new ArrayList<Integer>();
	private List<Integer> listIndexSpartans = new ArrayList<Integer>();
	private List<Integer> listIndexSpearman = new ArrayList<Integer>();
	private List<Integer> listIndexSteamGiant = new ArrayList<Integer>();
	private List<Integer> listIndexSwordman = new ArrayList<Integer>();
	private List<Integer> listIndexWall = new ArrayList<Integer>();
	
	public List<Integer> getListIndexArcher() {
		return listIndexArcher;
	}
	public List<Integer> getListIndexBalloonBombardier() {
		return listIndexBalloonBombardier;
	}
	public List<Integer> getListIndexCatapult() {
		return listIndexCatapult;
	}
	public List<Integer> getListIndexGyrocopter() {
		return listIndexGyrocopter;
	}
	public List<Integer> getListIndexHoplite() {
		return listIndexHoplite;
	}
	public List<Integer> getListIndexMarksman() {
		return listIndexMarksman;
	}
	public List<Integer> getListIndexMortar() {
		return listIndexMortar;
	}
	public List<Integer> getListIndexRam() {
		return listIndexRam;
	}
	public List<Integer> getListIndexSlinger() {
		return listIndexSlinger;
	}
	public List<Integer> getListIndexSpartans() {
		return listIndexSpartans;
	}
	public List<Integer> getListIndexSpearman() {
		return listIndexSpearman;
	}
	public List<Integer> getListIndexSteamGiant() {
		return listIndexSteamGiant;
	}
	public List<Integer> getListIndexSwordman() {
		return listIndexSwordman;
	}
	public List<Integer> getListIndexWall() {
		return listIndexWall;
	}
	public void setListIndexArcher(List<Integer> listIndexArcher) {
		this.listIndexArcher = listIndexArcher;
	}
	public void setListIndexBalloonBombardier(List<Integer> listIndexBalloonBombardier) {
		this.listIndexBalloonBombardier = listIndexBalloonBombardier;
	}
	public void setListIndexCatapult(List<Integer> listIndexCatapult) {
		this.listIndexCatapult = listIndexCatapult;
	}
	public void setListIndexGyrocopter(List<Integer> listIndexGyrocopter) {
		this.listIndexGyrocopter = listIndexGyrocopter;
	}
	public void setListIndexHoplite(List<Integer> listIndexHoplite) {
		this.listIndexHoplite = listIndexHoplite;
	}
	public void setListIndexMarksman(List<Integer> listIndexMarksman) {
		this.listIndexMarksman = listIndexMarksman;
	}
	public void setListIndexMortar(List<Integer> listIndexMortar) {
		this.listIndexMortar = listIndexMortar;
	}
	public void setListIndexRam(List<Integer> listIndexRam) {
		this.listIndexRam = listIndexRam;
	}
	public void setListIndexSlinger(List<Integer> listIndexSlinger) {
		this.listIndexSlinger = listIndexSlinger;
	}
	public void setListIndexSpartans(List<Integer> listIndexSpartans) {
		this.listIndexSpartans = listIndexSpartans;
	}
	public void setListIndexSpearman(List<Integer> listIndexSpearman) {
		this.listIndexSpearman = listIndexSpearman;
	}
	public void setListIndexSteamGiant(List<Integer> listIndexSteamGiant) {
		this.listIndexSteamGiant = listIndexSteamGiant;
	}
	public void setListIndexSwordman(List<Integer> listIndexSwordman) {
		this.listIndexSwordman = listIndexSwordman;
	}
	public void setListIndexWall(List<Integer> listIndexWall) {
		this.listIndexWall = listIndexWall;
	}

}
