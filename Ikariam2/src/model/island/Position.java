package model.island;

public class Position {
	private Cell[] arrayFrontLineCell;
	private Cell[] arrayAntiAircraftCell;
	private Cell[] arrayLongRangeCell;
	private Cell[] arrayFlanksCell;
	private Cell[] arrayArtilleryCell;
	private Cell[] arrayAircraftBombersCell;
	
	private int amountFontLine;
	private int amountAntiAircraft;
	private int amountLongRange;
	private int amountFlanks;
	private int amountArtillery;
	private int amountAircraftBombers;
	
	private int sizeFrontLine;
	private int sizeAntiAircraft;
	private int sizeLongRange;
	private int sizeFlanks;
	private int sizeArtillery;
	private int sizeAircraftBombers;
	
	
	public Position(Town townDefences) {
		
		if (townDefences.getLevel() >= 1 && townDefences.getLevel() <= 4) {
			amountFontLine = 3;
			amountLongRange = 3;
			amountFlanks = 0;
			amountArtillery = 1;
			amountAircraftBombers = 1;
			amountAntiAircraft = 1;
			
			sizeFrontLine = 30;
			sizeLongRange = 30;
			sizeFlanks = 0;
			sizeArtillery = 30;
			sizeAntiAircraft = 10;
			sizeAircraftBombers = 10;
		}else
		if (townDefences.getLevel() >= 5 && townDefences.getLevel() <= 9) {
			amountFontLine = 5;
			amountLongRange = 5;
			amountFlanks = 2;
			amountArtillery = 2;
			amountAircraftBombers = 1;
			amountAntiAircraft = 1;
			
			sizeFrontLine = 30;
			sizeLongRange = 30;
			sizeFlanks = 30;
			sizeArtillery = 30;
			sizeAntiAircraft = 20;
			sizeAircraftBombers = 20;
		}else
		if (townDefences.getLevel() >= 10 && townDefences.getLevel() <= 16) {
			amountFontLine = 7;
			amountLongRange = 7;
			amountFlanks = 4;
			amountArtillery = 3;
			amountAircraftBombers = 1;
			amountAntiAircraft = 1;
			
			sizeFrontLine = 30;
			sizeLongRange = 30;
			sizeFlanks = 30;
			sizeArtillery = 30;
			sizeAntiAircraft = 30;
			sizeAircraftBombers = 30;
		}else
		if (townDefences.getLevel() >= 17 && townDefences.getLevel() <= 24) {
			amountFontLine = 7;
			amountLongRange = 7;
			amountFlanks = 6;
			amountArtillery = 4;
			amountAircraftBombers = 2;
			amountAntiAircraft = 2;
			
			sizeFrontLine = 40;
			sizeLongRange = 40;
			sizeFlanks = 30;
			sizeArtillery = 30;
			sizeAntiAircraft = 20;
			sizeAircraftBombers = 20;
		}else
		if(townDefences.getLevel() >= 25) {
			amountFontLine = 7;
			amountLongRange = 7;
			amountFlanks = 6;
			amountArtillery = 5;
			amountAircraftBombers = 2;
			amountAntiAircraft = 2;
			
			sizeFrontLine = 50;
			sizeLongRange = 50;
			sizeFlanks = 40;
			sizeArtillery = 30;
			sizeAntiAircraft = 30;
			sizeAircraftBombers = 30;
		}
		
		this.arrayFrontLineCell = new Cell[amountFontLine];
		this.arrayAntiAircraftCell = new Cell[amountAntiAircraft];
		this.arrayLongRangeCell = new Cell[amountLongRange];
		this.arrayFlanksCell = new Cell[amountFlanks];
		this.arrayArtilleryCell = new Cell[amountArtillery];
		this.arrayAircraftBombersCell = new Cell[amountAircraftBombers];
		
	}
	
	
	
	public int getSizeFrontLine() {
		return sizeFrontLine;
	}



	public int getSizeAntiAircraft() {
		return sizeAntiAircraft;
	}



	public int getSizeLongRange() {
		return sizeLongRange;
	}



	public int getSizeFlanks() {
		return sizeFlanks;
	}



	public int getSizeArtillery() {
		return sizeArtillery;
	}



	public int getSizeAircraftBombers() {
		return sizeAircraftBombers;
	}



	public Cell[] getArrayFrontLineCell() {
		return arrayFrontLineCell;
	}



	public void setArrayFrontLineCell(Cell[] arrayFrontLineCell) {
		this.arrayFrontLineCell = arrayFrontLineCell;
	}



	public Cell[] getArrayAntiAircraftCell() {
		return arrayAntiAircraftCell;
	}



	public void setArrayAntiAircraftCell(Cell[] arrayAntiAircraftCell) {
		this.arrayAntiAircraftCell = arrayAntiAircraftCell;
	}



	public Cell[] getArrayLongRangeCell() {
		return arrayLongRangeCell;
	}



	public void setArrayLongRangeCell(Cell[] arrayLongRangeCell) {
		this.arrayLongRangeCell = arrayLongRangeCell;
	}



	public Cell[] getArrayFlanksCell() {
		return arrayFlanksCell;
	}



	public void setArrayFlanksCell(Cell[] arrayFlanksCell) {
		this.arrayFlanksCell = arrayFlanksCell;
	}



	public Cell[] getArrayArtilleryCell() {
		return arrayArtilleryCell;
	}



	public void setArrayArtilleryCell(Cell[] arrayArtilleryCell) {
		this.arrayArtilleryCell = arrayArtilleryCell;
	}



	public Cell[] getArrayAircraftBombersCell() {
		return arrayAircraftBombersCell;
	}



	public void setArrayAircraftBombersCell(Cell[] arrayAircraftBombersCell) {
		this.arrayAircraftBombersCell = arrayAircraftBombersCell;
	}



	public int getAmountFontLine() {
		return amountFontLine;
	}

	public int getAmountAntiAircraft() {
		return amountAntiAircraft;
	}

	public int getAmountLongRange() {
		return amountLongRange;
	}

	public int getAmountFlanks() {
		return amountFlanks;
	}

	public int getAmountArtillery() {
		return amountArtillery;
	}

	public int getAmountAircraftBombers() {
		return amountAircraftBombers;
	}
}
