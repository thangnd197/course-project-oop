package model.unit;

import javax.swing.ImageIcon;

public class Component {

	private int hitPoints;
	private int damage;
	private ImageIcon icon;
	
	
	
	public Component(int hitPoints, int damage, ImageIcon icon) {
		super();
		this.hitPoints = hitPoints;
		this.damage = damage;
		this.icon = icon;
	}
	public int getHitPoints() {
		return hitPoints;
	}
	public int getDamage() {
		return damage;
	}
	public ImageIcon getIcon() {
		return icon;
	}
	public void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}
	public void setDamage(int damage) {
		this.damage = damage;
	}
	public void setIcon(ImageIcon icon) {
		this.icon = icon;
	}

}
