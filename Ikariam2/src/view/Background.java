package view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.JPanel;


public class Background extends JPanel{

	private Image backgroundImage;
	
	Background(String linkImage) throws IOException{
		backgroundImage = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource(linkImage));
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(backgroundImage, 0, 0, this);
	}
	
}
