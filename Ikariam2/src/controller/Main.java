package controller;

import java.awt.EventQueue;

import view.IslandView;
import view.SetTownView;

public class Main {
  /**
   * Launch the application.
   */
  
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          IslandView frame = new IslandView();
          frame.setVisible(true);
//          SetTownView a = new SetTownView();
//          a.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
}
