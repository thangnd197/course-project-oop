package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedHashMap;

import model.Battle;
import model.FileIsland;
import model.InforOfCell;
import model.Island;
import model.unit.Archer;
import model.unit.BalloonBoombardier;
import model.unit.Boomber;
import model.unit.Catapult;
import model.unit.Cook;
import model.unit.Doctor;
import model.unit.Gyrocopter;
import model.unit.Hoplite;
import model.unit.Mortar;
import model.unit.Ram;
import model.unit.Slinger;
import model.unit.Spearman;
import model.unit.SteamGiant;
import model.unit.SulphurCarabineer;
import model.unit.Swordsman;
import model.unit.Unit;
import model.unit.Wall;
import model.unit.inforOfUnit;
import view.AttackArmySettingView;
import view.BattleView;

public class StartBattleController implements ActionListener {

  private AttackArmySettingView attackArmySettingView;
  
  public StartBattleController(AttackArmySettingView attackArmySettingView) {
    this.attackArmySettingView = attackArmySettingView;
  }

  @Override
  public void actionPerformed(ActionEvent e) {

    // Load island
    Island island = FileIsland.loadFile();
    int indexOfTown = attackArmySettingView.getIndexOfTown();
    
    LinkedHashMap<Unit, Integer> army = createArmy();
    
    Battle myBattle = new Battle(army, island.getTowns()[indexOfTown].getLevel(), false);
    
    Battle enermyBattle = new Battle(island.getTowns()[indexOfTown].getArmy(), 
        island.getTowns()[indexOfTown].getLevel(), true);
    
    // ================== truyen battle cho battle view
    try {
      BattleView b = new BattleView(myBattle, enermyBattle);
      attackArmySettingView.dispose();
    } catch (Exception e1) {
      e1.printStackTrace();
    }
    
  }

  private LinkedHashMap<Unit, Integer> createArmy() {
    LinkedHashMap<Unit, Integer> army = new LinkedHashMap<Unit, Integer>();
    //    "Hoplite",
    army.put(new Hoplite(inforOfUnit.HOPLITE[0], 
                         inforOfUnit.HOPLITE[1], 
                         inforOfUnit.HOPLITE[2], 
                         inforOfUnit.HOPLITE[3], 
                         inforOfUnit.HOPLITE[4], 
                         inforOfUnit.HOPLITE[5], 
                         inforOfUnit.HOPLITE[6]), 
             Integer.parseInt("0" + attackArmySettingView.getAmount()[0].getText().toString()));
    //    "Steam Giant",
    army.put(new SteamGiant(inforOfUnit.STEAM_GIANT[0], 
                          inforOfUnit.STEAM_GIANT[1], 
                          inforOfUnit.STEAM_GIANT[2], 
                          inforOfUnit.STEAM_GIANT[3], 
                          inforOfUnit.STEAM_GIANT[4], 
                          inforOfUnit.STEAM_GIANT[5], 
                          inforOfUnit.STEAM_GIANT[6]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[1].getText().toString()));
    //    "Sulphur Carabineer",
    army.put(new SulphurCarabineer(
                          inforOfUnit.SULPHUR_CARABINEER[0], 
                          inforOfUnit.SULPHUR_CARABINEER[1], 
                          inforOfUnit.SULPHUR_CARABINEER[2], 
                          inforOfUnit.SULPHUR_CARABINEER[3], 
                          inforOfUnit.SULPHUR_CARABINEER[4], 
                          inforOfUnit.SULPHUR_CARABINEER[5], 
                          inforOfUnit.SULPHUR_CARABINEER[6],
                          inforOfUnit.SULPHUR_CARABINEER[7]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[2].getText().toString()));
    //    "Archer",
    army.put(new Archer(inforOfUnit.ARCHER[0], 
                          inforOfUnit.ARCHER[1], 
                          inforOfUnit.ARCHER[2], 
                          inforOfUnit.ARCHER[3], 
                          inforOfUnit.ARCHER[4], 
                          inforOfUnit.ARCHER[5], 
                          inforOfUnit.ARCHER[6], 
                          inforOfUnit.ARCHER[7]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[3].getText().toString()));
    //    "Slinger",
    army.put(new Slinger(inforOfUnit.SLINGER[0], 
                          inforOfUnit.SLINGER[1], 
                          inforOfUnit.SLINGER[2], 
                          inforOfUnit.SLINGER[3], 
                          inforOfUnit.SLINGER[4], 
                          inforOfUnit.SLINGER[5], 
                          inforOfUnit.SLINGER[6],
                          inforOfUnit.SLINGER[7]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[4].getText().toString()));
    //    "Mortar",
    army.put(new Mortar(inforOfUnit.MORTAR[0], 
                          inforOfUnit.MORTAR[1], 
                          inforOfUnit.MORTAR[2], 
                          inforOfUnit.MORTAR[3], 
                          inforOfUnit.MORTAR[4], 
                          inforOfUnit.MORTAR[5], 
                          inforOfUnit.MORTAR[6],
                          inforOfUnit.MORTAR[7]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[5].getText().toString()));
    //    "Catapult",
    army.put(new Catapult(inforOfUnit.CATAPULT[0], 
                          inforOfUnit.CATAPULT[1], 
                          inforOfUnit.CATAPULT[2], 
                          inforOfUnit.CATAPULT[3], 
                          inforOfUnit.CATAPULT[4], 
                          inforOfUnit.CATAPULT[5], 
                          inforOfUnit.CATAPULT[6],
                          inforOfUnit.CATAPULT[7]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[6].getText().toString()));
    //    "Ram",
    army.put(new Ram(inforOfUnit.RAM[0], 
                          inforOfUnit.RAM[1], 
                          inforOfUnit.RAM[2], 
                          inforOfUnit.RAM[3], 
                          inforOfUnit.RAM[4], 
                          inforOfUnit.RAM[5], 
                          inforOfUnit.RAM[6],
                          inforOfUnit.RAM[5]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[7].getText().toString()));
    //    "Boombardier",
    army.put(new BalloonBoombardier(inforOfUnit.BOOMBARDIER[0], 
                          inforOfUnit.BOOMBARDIER[1], 
                          inforOfUnit.BOOMBARDIER[2], 
                          inforOfUnit.BOOMBARDIER[3], 
                          inforOfUnit.BOOMBARDIER[4], 
                          inforOfUnit.BOOMBARDIER[5], 
                          inforOfUnit.BOOMBARDIER[6],
                          inforOfUnit.BOOMBARDIER[7]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[8].getText().toString()));
    //    "Gyrocoper",
    army.put(new Gyrocopter(inforOfUnit.GYROCOPER[0], 
                          inforOfUnit.GYROCOPER[1], 
                          inforOfUnit.GYROCOPER[2], 
                          inforOfUnit.GYROCOPER[3], 
                          inforOfUnit.GYROCOPER[4], 
                          inforOfUnit.GYROCOPER[5], 
                          inforOfUnit.GYROCOPER[6],
                          inforOfUnit.GYROCOPER[7]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[9].getText().toString()));
    //    "Spearman",
    army.put(new Spearman(inforOfUnit.SPEARMAN[0], 
                          inforOfUnit.SPEARMAN[1], 
                          inforOfUnit.SPEARMAN[2], 
                          inforOfUnit.SPEARMAN[3], 
                          inforOfUnit.SPEARMAN[4], 
                          inforOfUnit.SPEARMAN[5], 
                          inforOfUnit.SPEARMAN[6]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[10].getText().toString()));
    //    "Swordsman",
    army.put(new Swordsman(inforOfUnit.SWORDSMAN[0], 
                          inforOfUnit.SWORDSMAN[1], 
                          inforOfUnit.SWORDSMAN[2], 
                          inforOfUnit.SWORDSMAN[3], 
                          inforOfUnit.SWORDSMAN[4], 
                          inforOfUnit.SWORDSMAN[5], 
                          inforOfUnit.SWORDSMAN[6]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[11].getText().toString()));
    //    "Cook",
    army.put(new Cook(inforOfUnit.COOK[0], 
                          inforOfUnit.COOK[1], 
                          inforOfUnit.COOK[2], 
                          inforOfUnit.COOK[3], 
                          inforOfUnit.COOK[4], 
                          inforOfUnit.COOK[5], 
                          inforOfUnit.COOK[6]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[12].getText().toString()));
    
    //    "Doctor"  
    army.put(new Doctor(inforOfUnit.DOCTOR[0], 
                          inforOfUnit.DOCTOR[1], 
                          inforOfUnit.DOCTOR[2], 
                          inforOfUnit.DOCTOR[3], 
                          inforOfUnit.DOCTOR[4], 
                          inforOfUnit.DOCTOR[5], 
                          inforOfUnit.DOCTOR[6]), 
            Integer.parseInt("0" + attackArmySettingView.getAmount()[13].getText().toString()));
    
    //     "Wall"
    army.put(new Wall(0, 0, 0, 0, 0, 0, 0, 0), 0);
    return army;
  }

}
