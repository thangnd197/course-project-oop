package model;

public class InforOfCell {
  
  public InforOfCell(int levelOfTown) {
    setUpCell(levelOfTown);
  }

  private int sizeFront;
  private int sizeLongRange;
  private int sizeArtilley;
  private int sizeBoom;
  private int sizePilot;
  private int sizeFlanks;
  private int amountFront;
  private int amountLongRange;
  private int amountArtilley;
  private int amountBoom;
  private int amountPilot;
  private int amountFlanks;
  
  private void setUpCell(int levelOfTown) {
    if (levelOfTown < 5) {
      sizeFront = 30;
      sizeLongRange = 30;
      sizeArtilley = 30;
      sizeBoom = 10;
      sizePilot = 10;
      sizeFlanks = 0;
      
      amountFront = 3;
      amountLongRange = 3;
      amountArtilley = 1;
      amountBoom = 1;
      amountPilot = 1;
      amountFlanks = 0;
    }else if (levelOfTown < 10) {
      sizeFront = 30;
      sizeLongRange = 30;
      sizeArtilley = 30;
      sizeBoom = 20;
      sizePilot = 20;
      sizeFlanks = 30;
      
      amountFront = 5;
      amountLongRange = 5;
      amountArtilley = 2;
      amountBoom = 1;
      amountPilot = 1;
      amountFlanks = 2;
    }else if (levelOfTown < 17) {
      sizeFront = 30;
      sizeLongRange = 30;
      sizeArtilley = 30;
      sizeBoom = 30;
      sizePilot = 30;
      sizeFlanks = 30;
      
      amountFront = 7;
      amountLongRange = 7;
      amountArtilley = 3;
      amountBoom = 1;
      amountPilot = 1;
      amountFlanks = 4;
    }else if (levelOfTown < 25) {
      sizeFront = 40;
      sizeLongRange = 40;
      sizeArtilley = 30;
      sizeBoom = 20;
      sizePilot = 20;
      sizeFlanks = 30;
      
      amountFront = 7;
      amountLongRange = 7;
      amountArtilley = 4;
      amountBoom = 2;
      amountPilot = 2;
      amountFlanks = 6;
    }else {
      sizeFront = 50;
      sizeLongRange = 50;
      sizeArtilley = 30;
      sizeBoom = 30;
      sizePilot = 30;
      sizeFlanks = 40;
      
      amountFront = 7;
      amountLongRange = 7;
      amountArtilley = 5;
      amountBoom = 2;
      amountPilot = 2;
      amountFlanks = 6;
    } 
  }
  
  public int getSizeFront() {
    return sizeFront;
  }

  public int getSizeLongRange() {
    return sizeLongRange;
  }

  public int getSizeArtilley() {
    return sizeArtilley;
  }

  public int getSizeBoom() {
    return sizeBoom;
  }

  public int getSizePilot() {
    return sizePilot;
  }

  public int getSizeFlanks() {
    return sizeFlanks;
  }

  public int getAmountFront() {
    return amountFront;
  }

  public int getAmountLongRange() {
    return amountLongRange;
  }

  public int getAmountArtilley() {
    return amountArtilley;
  }

  public int getAmountBoom() {
    return amountBoom;
  }

  public int getAmountPilot() {
    return amountPilot;
  }

  public int getAmountFlanks() {
    return amountFlanks;
  }
}
