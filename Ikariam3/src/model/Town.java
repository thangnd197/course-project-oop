package model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import model.unit.Unit;
import model.unit.Wall;

@SuppressWarnings("serial")
public class Town implements Serializable {

  public Town(LinkedHashMap<Unit, Integer> army, int level) { 
    this.setArmy(army);
    this.level = level;
  }
  
  public Town() {
  
  }
  /*
   *     "Hoplite",
    "Steam Giant",
    "Marksmen",
    "Archer",
    "Slinger",
    "Mortar",
    "Catapult",
    "Ram",
    "Boombardier",
    "Gyrocoper",
    "Spearman",
    "Swordsman",
    "Cook",
    "Doctor"
   */
  
  private LinkedHashMap<Unit, Integer> army;
  
  private int level;
  
  private Wall wall;
  
  // =============== Getter ====================== //
  public LinkedHashMap<Unit, Integer> getArmy() {
    return army;
  }

  public void setArmy(LinkedHashMap<Unit, Integer> army) {
    this.army = army;
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }
  
}
