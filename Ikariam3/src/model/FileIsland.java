package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileIsland {
  public FileIsland() {
    
  }
  
  public static void saveFile(Island island) {
    try {
      FileOutputStream fileOutputStream = new FileOutputStream("islandObject.txt");
      ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
      outputStream.writeObject(island);
      outputStream.close();
    }catch (IOException ex) {
      System.out.println("save file failed");
      return;
    }
    
  }
  
  public static Island loadFile() {
    try {
      FileInputStream fileInputStream = new FileInputStream("islandObject.txt");
      ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
      
      Island island = (Island) inputStream.readObject();   
      fileInputStream.close();
      return island;
    }catch (IOException ex ) {
      System.out.println("load file failed");
      return null;
    }catch (ClassNotFoundException ex) {
      System.out.println("load file failed");
      return null;
    }
    
    
  }
  
}
