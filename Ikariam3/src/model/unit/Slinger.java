package model.unit;

@SuppressWarnings("serial")
public class Slinger extends LongRangeFighter{

  public final String LINK = "/img/Slinger.gif";
  
  public Slinger(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage, int munition) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage, munition);
    // TODO Auto-generated constructor stub
  }
  @Override
  public String getLINK() {
    return LINK;
  }
}
