package model.unit;

@SuppressWarnings("serial")
public class Cook extends Support{

  public final String LINK = "/img/Cook.gif";
  
  public Cook(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }

  @Override
  public String getLINK() {
    return LINK;
  }
}
