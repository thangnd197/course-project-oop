package model.unit;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class Unit implements Serializable{
  public final String LINK = "";
  
  private int hitPoint;
  private int armor;
  private int size;
  private int rank;
  private int accuracy;
  private int speed;
  private int damage;
  
  public Unit(int hitPoint, int armour, int size,
      int rank, int accuracy, int speed, int damage) {
    this.hitPoint = hitPoint;
    this.armor = armour;
    this.size = size;
    this.rank = rank;
    this.accuracy = accuracy;
    this.speed = speed;
    this.damage = damage;
  }

  public int getHitPoint() {
    return hitPoint;
  }

  public int getArmor() {
    return armor;
  }

  public int getSize() {
    return size;
  }

  public int getRank() {
    return rank;
  }

  public int getAccuracy() {
    return accuracy;
  }

  public int getSpeed() {
    return speed;
  }

  public int getDamage() {
    return damage;
  }

  public void setHitPoint(int hitPoint) {
    this.hitPoint = hitPoint;
  }

  public void setArmor(int armor) {
    this.armor = armor;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public void setAccuracy(int accuracy) {
    this.accuracy = accuracy;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public void setDamage(int damage) {
    this.damage = damage;
  }
  
  public abstract String getLINK();
  
  
}
 