package model.unit;

@SuppressWarnings("serial")
public abstract class Artillery extends Unit {
  
  /**
   * Constructor
   * @param hitPoint
   * @param armour
   * @param size
   * @param rank
   * @param accuracy
   * @param speed
   * @param damage
   * @param munition
   */
  public Artillery(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage, int munition) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
    this.setMunition(munition);
  }
  
  private int munition;

  public int getMunition() {
    return munition;
  }

  public void setMunition(int munition) {
    this.munition = munition;
  }

}
