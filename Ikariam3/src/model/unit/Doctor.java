package model.unit;

@SuppressWarnings("serial")
public class Doctor extends Support{

  public final String LINK = "/img/Doctor.gif";
  
  public Doctor(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }

  @Override
  public String getLINK() {
    return LINK;
  }
}
