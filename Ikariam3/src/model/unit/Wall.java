package model.unit;

@SuppressWarnings("serial")
public class Wall extends Unit {
  
  public final String LINK = "/img/wall.gif";

  public Wall(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage,
      int levelOfWall) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
    this.levelOfWall = levelOfWall;
    this.garrison = calculateGarrison(levelOfWall);
  }
  
  
  private int levelOfWall;
  
  private int garrison;
  
  private int calculateGarrison(int levelOfWall) {
    switch (levelOfWall) {
      case 0: {
        this.garrison = 300;
        super.setHitPoint(0);
        super.setArmor(0);
        super.setDamage(0);
        break;
      }
      case 1: {
        this.garrison = 350;
        super.setHitPoint(150);
        super.setArmor(4);
        super.setDamage(20);
        break;
      }
      case 2: {
        this.garrison = 400;
        super.setHitPoint(200);
        super.setArmor(8);
        super.setDamage(20);
        break;
      }
      case 3: {
        this.garrison = 450;
        super.setHitPoint(250);
        super.setArmor(12);
        super.setDamage(20);
        break;
      }
      case 4: {
        this.garrison = 500;
        super.setHitPoint(300);
        super.setArmor(16);
        super.setDamage(20);
        break;
      }
      case 5: {
        this.garrison = 550;
        super.setHitPoint(350);
        super.setArmor(20);
        super.setDamage(20);
        break;
      }
      case 6: {
        this.garrison = 600;
        super.setHitPoint(400);
        super.setArmor(24);
        super.setDamage(20);
        break;
      }
      case 7: {
        this.garrison = 650;
        super.setHitPoint(450);
        super.setArmor(28);
        super.setDamage(20);
        break;
      }
      default: {
        this.garrison = 700;
        super.setHitPoint(500);
        super.setArmor(32);
        super.setDamage(20);
        break;
      }
    }
    
    return garrison;  
  }

  @Override
  public String getLINK() {
    // TODO Auto-generated method stub
    return LINK;
  }

}
