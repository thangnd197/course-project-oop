package model.unit;

@SuppressWarnings("serial")
public class Archer extends LongRangeFighter {
  
  public final String LINK = "/img/Archer.gif";

  public Archer(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage, int munition) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage, munition);
  }

  @Override
  public String getLINK() {
    return LINK;
  }

}
