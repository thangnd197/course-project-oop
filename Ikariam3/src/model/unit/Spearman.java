package model.unit;

@SuppressWarnings("serial")
public class Spearman extends LightInfantry{

  public final String LINK = "/img/Spearman.png";
  
  public Spearman(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }
  @Override
  public String getLINK() {
    return LINK;
  }
}
