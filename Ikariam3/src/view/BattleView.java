package view;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Battle;

public class BattleView {

  private JFrame frame;
  
  private JPanel panel;

  private JLabel[] frontLabel, longRangeLabel, artilleyLabel;
  
  private Battle myBattle, enermyBattle;
  
  public BattleView(Battle myBattle, Battle enermyBattle) {
    
    this.myBattle = myBattle;
    
    this.enermyBattle = enermyBattle;
    
    frontLabel = new JLabel[7];
    longRangeLabel = new JLabel[7];
    artilleyLabel = new JLabel[7];
    
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    frame = new JFrame();
    frame.setVisible(true);
    frame.setBounds(400, 150, 800, 600);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.getContentPane().setLayout(null);
    
    panel = new Background("/img/Big_battlefield.png");
    panel.setLayout(null);;
    panel.setBounds(0, 0, 800, 350);
    frame.getContentPane().add(panel);
    
    JButton btnNextRound = new JButton("Next round");
    btnNextRound.setBounds(650, 500, 117, 25);
    frame.getContentPane().add(btnNextRound);
    
    for (int i = 0; i < 2; i++) {
      
      // Boom
      if (myBattle.getBoom()[i] != null) {
        
        frontLabel[i] = new JLabel("");
        panel.add(frontLabel[i]);
        frontLabel[i].setBounds(142 + i * 49, 22, 31, 31);
        frontLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattle.getBoom()[i].getTroop().getLINK())
                )
        );
      }
      
      if (enermyBattle.getBoom()[i] != null) {
        
        frontLabel[i] = new JLabel("");
        panel.add(frontLabel[i]);
        frontLabel[i].setBounds(142 + i * 49, 291, 31, 31);
        frontLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattle.getBoom()[i].getTroop().getLINK())
                )
        );
      }
      
      // Pilot
      if (myBattle.getPilot()[i] != null) {
        
        frontLabel[i] = new JLabel("");
        panel.add(frontLabel[i]);
        frontLabel[i].setBounds(699 + i * 49, 22, 31, 31);
        frontLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattle.getPilot()[i].getTroop().getLINK())
                )
        );
      }
      
      if (enermyBattle.getPilot()[i] != null) {
        
        frontLabel[i] = new JLabel("");
        panel.add(frontLabel[i]);
        frontLabel[i].setBounds(699 + i * 49, 291, 31, 31);
        frontLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattle.getPilot()[i].getTroop().getLINK())
                )
        );
      }
      
    }
       
   
    for (int i = 0; i < 7; i++) {
      
      // my Battle =======================================
      // Front line
      if (myBattle.getMid()[0][i] != null) {
        
        frontLabel[i] = new JLabel("");
        panel.add(frontLabel[i]);
        frontLabel[i].setBounds(288 + i * 49, 120, 31, 31);
        frontLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattle.getMid()[0][i].getTroop().getLINK())
                )
        );
      }
      // Long Range line
      if (myBattle.getMid()[1][i] != null) {
        
        longRangeLabel[i] = new JLabel("");
        panel.add(longRangeLabel[i]);
        longRangeLabel[i].setBounds(288 + i * 49, 75, 31, 31);
        longRangeLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattle.getMid()[1][i].getTroop().getLINK())
                )
        );
      }
      
      // artilley line
      if (myBattle.getMid()[2][i] != null) {
        
        artilleyLabel[i] = new JLabel("");
        panel.add(artilleyLabel[i]);
        artilleyLabel[i].setBounds(288 + i * 49, 25, 31, 31);
        artilleyLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattle.getMid()[2][i].getTroop().getLINK())
                )
        );
      }
      
      // =======================================
      // enermy battle
      if (enermyBattle.getMid()[0][i] != null) {
        frontLabel[i] = new JLabel("");
        panel.add(frontLabel[i]);
        frontLabel[i].setBounds(288 + i * 49, 200, 31, 31);
        frontLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattle.getMid()[0][i].getTroop().getLINK())
                )
        ); 
      }
      
      // Long Range line
      if (enermyBattle.getMid()[1][i] != null) {
        
        longRangeLabel[i] = new JLabel("");
        panel.add(longRangeLabel[i]);
        longRangeLabel[i].setBounds(288 + i * 49, 245, 31, 31);
        longRangeLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattle.getMid()[1][i].getTroop().getLINK())
                )
        );
      }
      
      // artilley line
      if (enermyBattle.getMid()[2][i] != null) {
        
        artilleyLabel[i] = new JLabel("");
        panel.add(artilleyLabel[i]);
        artilleyLabel[i].setBounds(288 + i * 49, 295, 31, 31);
        artilleyLabel[i].setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattle.getMid()[2][i].getTroop().getLINK())
                )
        );
      }
      
      
      
      
    }
    
    
  }

}
