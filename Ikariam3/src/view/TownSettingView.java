package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.ApplyController;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import java.awt.Color;

public class TownSettingView extends JFrame {
  
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  private JPanel panel;
  
  private JTextField[] amount;
  private JComboBox<Integer>[] rankOfWeapon;
  private JComboBox<Integer>[] rankOfArmour;
  private JTextField textFieldWall;
  private JTextField textFieldTown;
  private JTextField textFieldSpies;
  private int indexOfTown;
  

  @SuppressWarnings("unchecked")
  public TownSettingView(int indexOfTown, IslandView islandView) {
    
    this.setIndexOfTown(indexOfTown);
    
    setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    setBounds(400, 150, 400, 600);
    
    contentPane = new JPanel();
    contentPane.setBackground(Color.LIGHT_GRAY);
    contentPane.setToolTipText("");
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    JLabel lblNewLabel = new JLabel("Troops");
    lblNewLabel.setBounds(46, 12, 70, 15);
    contentPane.add(lblNewLabel);
    
    panel = new JPanel();
    FlowLayout flowLayout = (FlowLayout) panel.getLayout();
    flowLayout.setAlignment(FlowLayout.LEFT);
    panel.setBounds(12, 39, 385, 505);
    contentPane.add(panel);
    // ====================================================
    // 
    this.amount = new JTextField[14];
    this.rankOfWeapon = new JComboBox [14];
    this.rankOfArmour = new JComboBox [14];
    
    for (int i = 0; i < 14; i++) {
      amount[i] = new JTextField();
      rankOfWeapon[i] = new JComboBox<>();
      rankOfArmour[i] = new JComboBox<>();
      
      setComponent(amount[i], rankOfWeapon[i], 
          rankOfArmour[i], Infor.NAME[i], Infor.LINK[i]);
    }
    setComponentLevelTown();
    setComponentWall();
    setComponentSpies();


    // ======================================================
    JButton btnAplly = new JButton("Apply");
    btnAplly.setBounds(140, 550, 117, 25);
    contentPane.add(btnAplly);
    btnAplly.addActionListener(new ApplyController(this, islandView)); 
    
	}


  private void setComponent(JTextField textAmount, 
      JComboBox<Integer> rankOfWeapon, 
      JComboBox<Integer> rankOfArmour,
      String name, String link) {
    // Ram
    Box unitBox = Box.createHorizontalBox();
    panel.add(unitBox);
    
    JLabel lblUnit = new JLabel(name);
    lblUnit.setPreferredSize(new Dimension(100, 20));
    unitBox.add(lblUnit);
    
    JLabel unitLabel = new JLabel("");
    unitLabel.setIcon(new ImageIcon(TownSettingView.class.getResource(link)));
    unitBox.add(unitLabel);
    
    textAmount.setPreferredSize(new Dimension(20, 20));
    textAmount.setColumns(10);
    unitBox.add(textAmount);
    
    unitBox.add(rankOfWeapon);

    unitBox.add(rankOfArmour);  
  }
  
  
  private void setComponentWall() {
    
    Box wallBox = Box.createHorizontalBox();
    panel.add(wallBox);
    
    JLabel lblLevelWall = new JLabel("Level Wall");
    lblLevelWall.setPreferredSize(new Dimension(100, 20));
    wallBox.add(lblLevelWall);
    
    JLabel wallLabel = new JLabel("");
    wallLabel.setIcon(new ImageIcon(TownSettingView.class.getResource("/img/wall.gif")));
    wallBox.add(wallLabel);
    
    textFieldWall = new JTextField();
    textFieldWall.setPreferredSize(new Dimension(20, 20));
    textFieldWall.setColumns(10);
    wallBox.add(textFieldWall);    
  }
  
  private void setComponentLevelTown() {
    
    Box townBox = Box.createHorizontalBox();
    panel.add(townBox);
    
    JLabel lblLevelTown = new JLabel("Level Town");
    lblLevelTown.setPreferredSize(new Dimension(100, 20));
    townBox.add(lblLevelTown);
    
    JLabel townLabel = new JLabel("");
    townLabel.setIcon(new ImageIcon(TownSettingView.class.getResource("/img/town.gif")));
    townBox.add(townLabel);
    
    textFieldTown = new JTextField();
    textFieldTown.setPreferredSize(new Dimension(20, 20));
    textFieldTown.setColumns(10);
    townBox.add(textFieldTown);    
  }
  
  private void setComponentSpies() {
    
    Box spiesBox = Box.createHorizontalBox();
    panel.add(spiesBox);
    
    JLabel lblDoctor = new JLabel("Spies");
    lblDoctor.setPreferredSize(new Dimension(100, 20));
    spiesBox.add(lblDoctor);
    
    JLabel spiesLabel = new JLabel("");
    spiesLabel.setIcon(new ImageIcon(TownSettingView.class.getResource("/img/spy.gif")));
    spiesBox.add(spiesLabel);
    
    textFieldSpies = new JTextField();
    textFieldSpies.setPreferredSize(new Dimension(20, 20));
    textFieldSpies.setColumns(10);
    spiesBox.add(textFieldSpies);    
  }
  
  // ============= Getter ===================
  public JTextField[] getAmount() {
    return amount;
  }

  public JComboBox<Integer>[] getRankOfWeapon() {
    return rankOfWeapon;
  }

  public JComboBox<Integer>[] getRankOfArmour() {
    return rankOfArmour;
  }

  public JTextField getTextFieldWall() {
    return textFieldWall;
  }

  public JTextField getTextFieldTown() {
    return textFieldTown;
  }

  public JTextField getTextFieldSpies() {
    return textFieldSpies;
  }


  public int getIndexOfTown() {
    return indexOfTown;
  }


  public void setIndexOfTown(int indexOfTown) {
    this.indexOfTown = indexOfTown;
  }
}
